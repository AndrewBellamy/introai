---
title: "Task 6.2"
author: "Andrew Bellamy"
date: May 13, 2020
output:
  pdf_document:
    toc: true
    toc_depth: 2
    fig_caption: true
  html_document:
    theme: darkly
    toc: true
    toc_depth: 2
    toc_float: true
  word_document: default
---

# Tasks
The following subtasks continue the topic of classification, drawing on the previous task usage of Decision Trees and Binary Logistic Regression Models, and introducing K-Nearest Neighbor classification.

## Load Dataset and Installing Libraries
We continue with the *Smarket* dataset from the *ISLR* library. Using classification, we'll attempt to predict the *Direction* variables values of either _Up_ or _Down_, but instead of random sampling, time series will be taken into account. Since we'll be fitting and plotting a Decision Tree and creating K-NN classifiers, it is also worth installing the libraries *rpart*, *rpart.plot* and *FNN*.

```{r, warning=FALSE, message=FALSE}
# Load Dataset
#install.packages("ISLR")

library("ISLR")
head(Smarket)

#install.packages("rpart")
library(rpart)
#install.packages("rpart.plot")
library(rpart.plot)
#install.packages("FNN")
library(FNN)
```

## Train and Test Subsets
In the previous task, I'd used random sampling to extract the indices for the training and test subsets. In this task I will be simply taking the first 80% of rows in the dataset for the training subset and leaving the remaining 20% for the test subset. I created a range from the first index to the index at the 80% position, then used the range of indices to assign each subset. 

```{r, warning=FALSE, message=FALSE}
# Train and Test Subset
train_ind <- 1:floor(nrow(Smarket)*0.8)

Smarket_train <- Smarket[train_ind,]
Smarket_test <- Smarket[-train_ind,]

sprintf("Data split for train/test = %i:%i",nrow(Smarket_train),nrow(Smarket_test))
```

## Decision Tree
Similar to the previous task, I'll fit a Decision Tree to model *Direction* as a function of *Lag1*, *Lag2*, *Lag3*, *Lag4* and *Lag5* but with a different training subset, the results will vary. We can see from the plot that the classification split rule is different from the previous task, with an almost even split based on the rule:

$$Lag1 \ge 0.05$$
If the rule is met, the value for *Direction* is classified as _Down_, with _Up_ calssified for the opposite.

```{r, warning=FALSE, message=FALSE}
# Tree and Plot
d_tree <- rpart(
  Direction~Lag1+Lag2+Lag3+Lag4+Lag5, 
  data=Smarket_train, 
  method="class", 
  cp=0.05)

rpart.plot(d_tree, varlen=0, tweak=1.1, fallen.leaves=FALSE, digits=3)
```

### Evaluation
Using the method from the prescribed text *get_metrics*, we can retrieve all the performance metrics for the Decision Tree model, as well as the values of the confusion matrix.

```{r, warning=FALSE, message=FALSE}
# Evaluate Decision Tree
get_metrics <- function(Y_pred, Y_test)
{
  C <- table(Y_pred, Y_test) # confusion matrix
  stopifnot(dim(C) == c(2, 2))
  c(Acc=(C[1,1]+C[2,2])/sum(C), # accuracy
    Prec=C[2,2]/(C[2,2]+C[2,1]), # precision
    Rec=C[2,2]/(C[2,2]+C[1,2]), # recall
    F=C[2,2]/(C[2,2]+0.5*C[1,2]+0.5*C[2,1]), # F-measure
    # Confusion matrix items:
    TN=C[1,1], FN=C[1,2],
    FP=C[2,1], TP=C[2,2]
  ) # return a named vector
}
dir_pred <- predict(d_tree, Smarket_test, type = "class")
get_metrics(dir_pred,Smarket_test[,"Direction"])
```

## Binary Logistic Regression Models
In this scenario, instead of one Binary Logistic Regression Model for all the independent variables (*Lag1*, *Lag2*, *Lag3*, *Lag4*, *Lag5*), we'll create 5 models for *Direction* as a function of each independent variable and observe the coefficients. I created a function, *binlog_factory* which would accept the dependent variable, a vector of independent variables and the dataset. For each independent variable, a model is assigned and the coefficients extracted.

```{r, warning=FALSE, message=FALSE}
# Binary Logistic Models
binlog_factory = function(dep_var, ind_col, dataset) {
  for(ind_var in ind_col) {
    mdl <- glm(dataset[,dep_var]~dataset[,ind_var], family=binomial("logit"))
    print(sprintf("%s ~ %s",dep_var,ind_var))
    print(mdl$coefficients)
  }
}

binlog_factory("Direction",c("Lag1","Lag2","Lag3","Lag4","Lag5"),Smarket_train)
```

## K-Nearest Neighbors Classification
Lastly, we look at K-Nearest Neighbor classifiers which use a plurality vote of _k_ closest neighbors on a Euclidean Plane, that is, the common class among the _k_ nearest neighbors, in order to classify each object. We'll be using two values for _k_, _5_ and _15_.

### K = 5
To assist in separating the training and test predictors, I assigned a vector of the relevant column names. Then it was a simple matter of using the *knn* function with the training predictors, the test predictors, the training classes and the value of _k_ as parameters. Finally, I used the *get_metrics* function to evaluate the performance of the classifier, based on the predictions for *Direction* in the test subset.

```{r, warning=FALSE, message=FALSE}
# K-NN 5
predictors <- c("Lag1","Lag2","Lag3","Lag4","Lag5")

dir_knn_5 <- knn(
  Smarket_train[,predictors], 
  Smarket_test[,predictors], 
  Smarket_train[,"Direction"], 
  k=5
  )
get_metrics(dir_knn_5,Smarket_test[,"Direction"])
```

### K = 15
For the second classifier, the process was repeated with _k_ set to _15_ in the *knn* function.

```{r, warning=FALSE, message=FALSE}
# K-NN 15
dir_knn_15 <- knn(
  Smarket_train[,predictors], 
  Smarket_test[,predictors], 
  Smarket_train[,"Direction"], 
  k=15
  )
get_metrics(dir_knn_15,Smarket_test[,"Direction"])
```

## Inferences
When looking at the accuracy of the Decision Tre and the K-NN classifiers, we can see that the Decision tree is most accurate with 0.55, followed by 5-NN at 0.53, and with 15-NN coming last at 0.52. These are all too low for any _real_ confidence in correctly predicting the value of *Direction* but are useful for comparing.

We can compare other evaluation metrics aside from accuracy, these include Precision, Recall and F-Measure. Precision is the proportion of data points that have been correctly classified as belonging to the target class, among all the cases that have been classified as belonging to the target class, calculated as:

$$Precision = \frac{TP}{TP+FP}$$
We can see that precision for the Decision Tree is highest at 0.62. Similar to accuracy the K-NN classifiers trail the Decision Tree in precision, with 5-NN at 0.58 and 15-NN at 0.56. Recall, on the other hand, is the opposite. Recall, also known as *Sensitivity*, is the proportion of data points that have been correctly classified as belonging to the target class, among all the cases that are _actually_ in the target class; and is calculated:

$$Recall = \frac{TP}{TP+FN}$$
We can see that the Decision Tree has the lowest recall at 0.54, this increases with the K-NN classifiers with 5 and 15 at 0.59 and 0.61, respectively. Without a comparison goal (higher precision v. higher recall) for these metrics, it's difficult to know which technique performs better; for that we can use the F-Measure. F-Measure is the harmonic mean of precision and recall, and is calculated:

$$F1 = 2*\frac{precision * recall}{precision + recall}$$
We can see that Decision Tree has the lowest F-Measure with 0.57, followed by 5-NN with 0.58, and 15-NN with 0.59.

As for the coefficients for each of the Binary Logistic Models, we can see that from *Lag1* to *Lag5* the significance of their impact on the logarithm of the odds ratio reduces, though they are all insignificant as they are all very close to 0; similar to the Binary Logistic Regression Model from the previous task.
