# Base X Axis
x <- seq(0, 10)

# Y sequences; traditional, small, medium and large
t <- x^0.1
s <- x^0.25
m <- x^0.5
l <- x^0.75

# Plot plane and additional lines
plot(x, l, type="l",
     lwd=2, col="Red",
     xaxt="n",yaxt="n", main="Drivers of Deep Learning",
     xlab="Data", ylab="Performance")
lines(x, m, lwd=2, col="Yellow")
lines(x, s, lwd=2, col="Green")
lines(x, t, lwd=2, col="Blue")

# Line Labels
text(max(x), max(l), "Large NN", pos=2)
text(max(x), max(m), "Medium NN", pos=2)
text(max(x), max(s), "Small NN", pos=2)
text(max(x), max(t), "Traditional AI", pos=2)