# install.packages("ISLR")

# Load ISLR package
library("ISLR")
head(Auto, 3)

# Linear Model
model <- lm(mpg~horsepower, Auto)
logmodel <- lm(log(mpg)~horsepower, Auto)
summary(model)

# Regression Line
plot(Auto$horsepower, Auto$mpg,
               main="Simple Regression of MPG as function of Horsepower",
               xlab="Horsepower", ylab="Miles per Gallon",
               col="Orange", lwd=2)
abline(model)
lines(Auto$horsepower, predict(logmodel), col="purple")

# Coefficient of Determination
determ <- summary(model)$r.squared

# Predictions
model$coefficients[1] + model$coefficients[2]*125
model$coefficients[1] + model$coefficients[2]*300

predict(model, data.frame((horsepower=c(125, 300))))


